import React from 'react';
import './App.css';
import Navbar from './components/layout/Navbar'
import Login from './components/pages/Login'
import Home from './components/pages/Home'
import Register from './components/pages/Register'
import UserState from './context/User/UserState'
import { BrowserRouter as Router , Switch , Route} from 'react-router-dom'

const App = ()=> {

  return (
    <UserState>
      <Router>
        <div className="App">
          <Navbar></Navbar>
          <Switch>
            <Route exact path="/" component={Home}></Route>
            <Route exact path="/login" component={Login}></Route>
            <Route exact path="/register" component={Register}></Route>
            <Route exact path="/home" component={Home}></Route>
          </Switch>
        </div>
      </Router>
    </UserState>
  );
}

export default App;

import React,{useContext , useEffect, Fragment} from 'react'
import UserContext from '../../context/User/UserContext'
import Contacts from '../users/Contacts'
import { Link } from 'react-router-dom'

const Home = () => {


    const userContext = useContext(UserContext)

    const {currentUser , contacts} = userContext

    useEffect(() => {
        
    })

    return (
        <Fragment>
            {currentUser !== null ? (<Fragment>
                    <Contacts contacts={contacts}></Contacts>
            </Fragment>) : ( 
                <p id="HomeText" className="text-muted text-center">
                    Please <Link to="/login" className="text-success">LOGIN</Link> to see the contacts.
                </p>
            )}
        </Fragment>
    )
}


export default Home

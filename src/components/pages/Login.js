import React, { useContext, useState, Fragment } from 'react'
import {useHistory} from 'react-router-dom'
import UserContext from '../../context/User/UserContext'


const Login = () => {

    const userContext = useContext(UserContext)
    const {Login , setCurrent , users} = userContext

    const History = useHistory();

    const [user, setUser] = useState({
        email: '',
        password: '',
    })
    const [error , setError] = useState([])

    const { 
        email,
        password,
    } = user;

    const onChange = (e) => {
        setUser({
            ...user,
            [e.target.name]: e.target.value
        })
    }

    const onSubmit = (e) => {
        e.preventDefault();
        const targetUser = users.filter(user => {
            return email === user.email && password === user.password
        })
        if (targetUser.length > 0) {
            Login(user);
            setCurrent(user);
            History.push('/home')

        } else {
            setError('Email or Password is incorrect')
            setTimeout(() => {
                setError([])
            }, 2000);
        }
    }

    return (
        <div id="loginForm" className="container">
            <form action="" onSubmit={onSubmit}>
                <h2 className="text-center mb-5">Login to Your Account</h2>
                {error.length > 0 ? <div className="errors bg-danger text-light p-2 m-3" style={{ borderRadius: '5px'}}>{error}</div> : <Fragment></Fragment>}
                <div className="form-group">
                    <label htmlFor="Email">Email Adress:</label>
                    <input onChange={onChange} className="form-control" name="email" type="text" value={email}/>
                </div>
                <div className="form-group">
                    <label htmlFor="password">Password:</label>
                    <input onChange={onChange} className="form-control" type="password" name="password" value={password}/>
                </div>
                <button  type="submit" className="btn btn-success btn-block mt-5">Login</button>
            </form>
        </div>
    )
}


export default Login

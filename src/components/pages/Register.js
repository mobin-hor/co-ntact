import React, { useState, useContext , Fragment } from 'react'
import { useHistory } from 'react-router-dom'
import UserContext from '../../context/User/UserContext'

const Register = () => {
        
    const userContext = useContext(UserContext)

    const History = useHistory()

    const [user, setUser] = useState({
        name: '',
        email: '',
        password: '',
        password2: '',
        contacts:[],
    })

    const [Registered , setRegistered] = useState(false)

    const [errors , setError] = useState([])

    const { name, email, password, password2 } = user;

    const onChange = (e) => {
        
        setUser({ ...user, [e.target.name]: e.target.value })
    }

    const onSubmit = (e) => {
        e.preventDefault();
        if (name.length < 5) {
            setError(errors => [...errors, {msg:'Name Should be more than 5 letters'}])
        }
        if (password.length < 5 && password2.length < 5) {
           setError(errors => [...errors, {msg:'Password should be more than 5 characters'}])
        }
        if (password !== password2) {
            setError(errors => [...errors, {msg:'Passwords dont match'}])
        }

        
        if (name.length >= 5 && password.length >= 5 && password2.length>=5 && password===password2) {
            userContext.Register(user)
            setRegistered(true)
            setError([])
            console.log(Registered)
            setTimeout(() => {
                setRegistered(false)
                History.push('/login')
            }, 3000);

        } else {
            setTimeout(() => {
                setError([])
            }, 5000);
        }
    }

    return (
        <div id="registerForm" className="container">
            <form action="" onSubmit={onSubmit}>
                <h2 className="text-center mb-5">Register an Account</h2>
                {Registered === true ? <p className="errors bg-success text-light p-2 m-3" style={{ borderRadius: '5px'}}>Registered Successfully , Login and Continue</p> : <Fragment></Fragment>}
                {errors.length > 0 ? errors.map(error => (<p key={error.msg} className="errors bg-danger text-light p-2 m-3" style={{ borderRadius: '5px'}}>{error.msg}</p>)) : <Fragment></Fragment>}
                <div className="form-group">
                    <label htmlFor="Name">Name:</label>
                    <input onChange={onChange} className="form-control" type="text" name="name" value={name}/>
                </div>
                <div className="form-group">
                    <label htmlFor="Email">Email Adress:</label>
                    <input onChange={onChange} className="form-control" type="text" name="email" value={email}/>
                </div>
                <div className="form-group">
                    <label htmlFor="password">Password:</label>
                    <input onChange={onChange} className="form-control" type="password" name="password" value={password}/>
                </div>
                <div className="form-group">
                    <label htmlFor="password2">Confirm Password:</label>
                    <input onChange={onChange} className="form-control" type="password" name="password2" value={password2}/>
                </div>
                <button  type="submit" className="btn btn-success btn-block mt-5">Register</button>
            </form>
        </div>
    )
}

export default Register
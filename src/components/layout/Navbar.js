import React ,{Fragment , useContext} from 'react'
import UserContext from '../../context/User/UserContext'
import {Link} from 'react-router-dom'

export default function Navbar() {

    const userContext = useContext(UserContext)

    const { currentUser , logOut } = userContext
    
    return (
        <Fragment>
                <div id="navbar" className="row align-items-center">
                    <div className="logo col-md-10 col-sm-12">
                    </div>
                    <div className="col-md-2 col-sm-12">
                        <ul className="d-flex" >
                            <li>
                                {currentUser===null ? <Link to="/login" tag="button" className="btn btn-success text-center">Login</Link> : (<Fragment><h5>{currentUser.name} </h5><Link to="/" onClick={logOut} tag="button" className="btn btn-danger">LogOut</Link></Fragment> )}
                            </li>
                            <li>
                                {currentUser===null ? <Link to="/register" tag="button" className="btn btn-dark">Register</Link> : <Fragment></Fragment> }
                            </li>
                        </ul>
                    </div>
                </div>
        </Fragment>
    )
}

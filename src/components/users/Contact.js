import React,{useContext , useState} from 'react'
import UserContext from '../../context/User/UserContext'


const Contact = (props) => {

    const { name, email, phone } = props.contact

    const [contact, setContact] = useState({
        email,
        name,
        phone,
    })

    const userContext = useContext(UserContext)

    const { removeContact , updateContact } = userContext


    const onDelete = () => {
        removeContact(props.contact)
    }

    const onChange = (e) => {
        setContact({
            ...contact,
            [e.target.name]: e.target.value
        })
    }

    const onEdit = (e) => {
        e.preventDefault();
        updateContact(contact)
    }

    return (
        <div>
            <div id="contactCard" className="card border-primary mt-5" style={{maxWidth:500}}>
                <div className="card-header text-center"><h4>{name}</h4></div>
                    <div className="card-body text-dark">
                    <h5 className="card-title">Email : {email}</h5>
                    <p className="card-text">Phone: {phone}</p>
                    </div>
                    <button type="button" className="btn btn-success m-1" data-toggle="modal" data-target="#editForm">Edit</button>
                    <button onClick={onDelete} className="btn btn-danger m-1">Remove</button>
            </div>
                        <div className="modal fade" id="editForm" tabIndex="-1" role="dialog" aria-labelledby="editFormLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="editFormLabel">Edit the Contact</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <form  action="">
                                <div className="form-group">
                                    <label htmlFor="email">Email</label>
                                    <input onChange={onChange} className="form-control" type="email" name="email" value={contact.email} disabled/>
                                    <small className="text-danger">email cant be changed</small>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="name">Name</label>
                                     <input onChange={onChange} className="form-control" type="text" name="name" value={contact.name}></input>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="phone">Phone</label>
                                    <input onChange={onChange} className="form-control" type="text" name="phone" value={contact.phone}/>
                                </div>
                            </form>
                        </div>
                        <div className="modal-footer">
                            <button onClick={onEdit} type="button" className="btn btn-primary btn-block" data-dismiss="modal">Update Contact</button>
                            <button type="button" className="btn btn-secondary btn-block" data-dismiss="modal">Close</button>
                            
                        </div>
                    </div>
                </div>
            </div>

        </div>
    )
}

export default Contact

import React, { Fragment } from 'react'
import Contact from './Contact.js'
import AddContact from './AddContact.js'


const Contacts = (props) => {


    const { contacts } = props



    return (
        <div id="contacts" className="container">
            <div className="row">
                <div className="col-md-6">
                {contacts.length !== 0 ? (<Fragment>
                        {contacts.map(contact => (
                        <Contact key={contact.email} contact={contact}></Contact>
                    ))}
                </Fragment>):<h1 className="text-center mt-5">You Have No Contacts</h1>}
                </div>
                <div className="col-md-6">
                    <AddContact></AddContact>
                </div>
            </div>    
        </div>    
    )
  

}

export default Contacts

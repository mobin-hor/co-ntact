import React, { Fragment, useState , useContext} from 'react'
import UserContext from '../../context/User/UserContext'

const AddContact = () => {

    const userContext = useContext(UserContext)

    const {
        addContact,
    } = userContext

    const [contact, setContact] = useState({
        name : '',
        email : '',
        phone : ''
    })
    
    const [errors, setError] = useState([])


    const { name, email, phone } = contact;


    const onChange = (e) => {
        setContact({
            ...contact,
            [e.target.name]: e.target.value
        })
    }

    const onSubmit = (e) => {
        e.preventDefault();
        if (name.length < 5) {
            setError(errors => [...errors, {msg:'Name Should be more than 5 letters'}])
        }
        if (email.length===0) {
            setError(errors => [...errors, {msg:'Enter a correct Email'}])
        }
        if (phone.length!==11) {
            setError(errors => [...errors, {msg:'Phone number Should be 11 digits'}])
        }
        if (name.length >= 5 && phone.length === 11) {
            addContact(contact);
            setError([])
            setContact({
                name: '',
                email: '',
                phone:''
            })
        
        } else {
            setTimeout(() => {
                setError([])
            }, 5000);
        }
    }
    


    return (
        <Fragment>
                <form onSubmit={onSubmit} id="AddForm">
                    <h3 className="text-center">Add New Contact</h3>
                    {errors.length > 0 ? errors.map(error => (<small key={error.msg} className="errors bg-danger text-light p-2 m-3" style={{ borderRadius: '5px',display:'block'}}>{error.msg}</small>)) : <Fragment></Fragment>}
                    <div className="form-group">
                        <label htmlFor="name">Name:</label>
                    <input onChange={onChange} className="form-control" type="text" name="name" value={name} placeholder="Contact Name..."></input>
                    </div>
                    <div className="form-group">
                        <label htmlFor="email">Email:</label>
                        <input onChange={onChange} className="form-control" type="email" name="email" value={email} placeholder="Contact Email..."></input>
                    </div>
                    <div className="form-group">
                        <label htmlFor="Phone">Phone</label>
                        <input onChange={onChange} className="form-control" type="text"  name="phone" value={phone} placeholder="Contact Phone..."></input>
                    </div>
                    <button  className="btn btn-info btn-block" type="submit" value="submit">Add Contact</button>
                </form>
        </Fragment>
    )
}

export default AddContact
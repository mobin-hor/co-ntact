import {
    REGISTER_USER,
    LOGIN_USER,
    GET_CONTACTS,
    SET_CURRENT,
    ADD_CONTACT,
    REMOVE_CONTACT,
    UPDATE_CONTACT,
    LOG_OUT,
} from '../types'


export default (state, action) => {
    switch (action.type) {
        
        case REGISTER_USER: return {
            ...state,
            users: [...state.users, action.payload]
        }

        case LOGIN_USER: return {
            ...state,
            currentUser: state.users.filter((user) => {
                return (user.email === action.payload.email && user.password === action.payload.password)
            })
        }

        case SET_CURRENT: return {
            ...state,
            currentUser: action.payload
        }

        case GET_CONTACTS: return {
            ...state,
            contacts: state.currentUser.contacts
        }
        case ADD_CONTACT: {
            state.users.forEach(user => {
                if (user.email === state.currentUser.email) {
                    user.contacts.push(action.payload)
                }
            })
            return {
                ...state,
                users: state.users,
            }
        }
        case REMOVE_CONTACT: {
            let newContacts;
            state.users.forEach(user => {
                if (user.email === state.currentUser.email) {
                    newContacts = user.contacts.filter(contact => {
                        return contact !== action.payload
                    })
                    user.contacts = newContacts
                }
            })
            return {
                ...state,
                users: state.users,
                contacts: newContacts
            }
        }
            
        case UPDATE_CONTACT: {
            state.users.forEach(user => {
                if (user.email === state.currentUser.email) {
                    user.contacts.forEach(contact => {
                        if (contact.email === action.payload.email) {
                            contact.name = action.payload.name;
                            contact.phone = action.payload.phone
                        }
                    })
                }
            })
        
            return {
                ...state,   
                users: state.users,
                contacts: state.contacts,
        }
        }
            
        case LOG_OUT: return {
            ...state,
            currentUser : null,
        }


        default: return{ ...state}
    }
}
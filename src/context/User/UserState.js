import React, { useReducer } from 'react'
import UserContext from './UserContext'
import UserReducer from './UserReducer'
import {
    REGISTER_USER,
    LOGIN_USER,
    GET_CONTACTS,
    SET_CURRENT,
    ADD_CONTACT,
    REMOVE_CONTACT,
    UPDATE_CONTACT,
    LOG_OUT,
} from '../types'



const UserState = props => {
    const initialState = {
        users: [],
        currentUser: null,
        contacts: [],
    }


    const [ state , dispatch ] = useReducer(UserReducer , initialState )


    // Register User
    const Register = (user) => {
        dispatch({
            type: REGISTER_USER,
            payload: user,
        })
    }
    // Login user


    const Login = (user) => {      
        dispatch({
            type: LOGIN_USER,
            payload:user,
        })
    }

    // logOut user
    const logOut = () => {
        dispatch({
            type:LOG_OUT,
        })
    }
    


    // set current user
    const setCurrent = (domUser) => {
        const target = state.users.filter(user => {
            return user.email===domUser.email && user.password===domUser.password
        })

        if(target.length > 0) {
            dispatch({
                type: SET_CURRENT,
                payload:target[0],
            })
            getContacts()

        } else {
            console.log("Error!!!");
        }
    }


    // getting current users contacts
    const getContacts = () => {
        dispatch({
            type:GET_CONTACTS
        })
    }


    // add Contact

    const addContact = (contact) => {
        dispatch({
            type: ADD_CONTACT,
            payload:contact
        })
    }

    // Remove Contact

    const removeContact = (contact) => {
        dispatch({
            type: REMOVE_CONTACT,
            payload:contact
        })
    }

    // update contact 

    const updateContact = (contact) => {
        dispatch({
            type: UPDATE_CONTACT,
            payload:contact
        })
    }



    return (
        <UserContext.Provider
            value={{
                users: state.users,
                currentUser: state.currentUser,
                contacts: state.contacts,  
                Register,
                Login,
                setCurrent,
                getContacts,
                addContact,
                removeContact,
                updateContact,
                logOut,
            }}>
        {props.children}
        </UserContext.Provider>
    )
}


export default UserState